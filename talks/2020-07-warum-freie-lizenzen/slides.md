# Warum freie Lizenzen?
Alex Steckel

<small>
Göttingen State and University Library

[steckel@sub.uni-goettingen.de](steckel@sub.uni-goettingen.de)
</small>

---

Zu beantwortende Fragen:
- Was sind offene Lizenzen?
- Welche Argumente sprechen für eine offene Lizenzierung?
- Welche Initiativen und Projekte existieren schon in den Organisationen/Institutionen?

--

![an image](img/some-image.png)

you can insert Images…

[…and Hyperlinks](https://de.dariah.eu/)

--

![from wikicommons](https://upload.wikimedia.org/wikipedia/commons/8/8b/Campsite_at_Mystic_Beach%2C_Vancouver_Island%2C_Canada.jpg)


--


Sources:

| ![[Open Practical Guide to Using Creative Common Licenses](https://commons.wikimedia.org/wiki/File:Open_Content_A_Practical_Guide_to_Using_Creative_Commons_Licences_web.pdf)](img/Open_Content_-_Ein_Praxisleitfaden_zur_Nutzung_von_Creative-Commons-Lizenzen-1_title.png) | ![[Made with CC](https://creativecommons.org/wp-content/uploads/2017/04/made-with-cc.pdf)](img/made-with-cc-1_title.png)|
|---|---|
|[Open Practical Guide to Using Creative Common Licenses](https://commons.wikimedia.org/wiki/File:Open_Content_A_Practical_Guide_to_Using_Creative_Commons_Licences_web.pdf)|[Made with CC](https://creativecommons.org/wp-content/uploads/2017/04/made-with-cc.pdf)|

--

## Headline 2

- Lists
  - sublists
  - yes
- another point
- and give me smiles please: :smile: :smiley: :cry: :wink:

--

### Headline 3

Some awesome text here.

--

<!-- .slide: data-background-color="black" -->

## Background Color Makes Life Good

--

<!-- .slide: data-background-image="img/some-image.png" -->

### Background Image

---

What about a table? You can easily create one with the help of [this tool](https://www.tablesgenerator.com/markdown_tables).

| Head 1 | Head 2 | Head 3 |
|----------|--------|--------|
| centered | left | right |
| content | align | align |
